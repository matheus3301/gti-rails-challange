Rails.application.routes.draw do
  resources :reservations
  resources :librarians
  resources :books
  devise_for :users
  get 'home/index'
  resources :clients
  resources :categories
  resources :authors

  root 'reservations#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

end
