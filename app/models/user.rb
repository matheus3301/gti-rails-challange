class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable

  after_create :create_librarian

  after_destroy :destroy_librarian

  def create_librarian
    Librarian.create!(email: self.email)
  end

  def destroy_librarian
    Librarian.where(email: self.email).destroy_all
  end

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
end
