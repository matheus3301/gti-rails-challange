class Reservation < ApplicationRecord
  belongs_to :book
  belongs_to :client
  belongs_to :librarian

  after_create :decrease_book_stock
  after_destroy :increase_book_stock

  def decrease_book_stock
    self.book.update(stock: self.book.stock - 1)
  end

  def increase_book_stock
    self.book.update(stock: self.book.stock + 1)
  end

end
