# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


category_list = [
  "Romance",
  "Acadêmico",
  "Literatura",
  "Policial",
  "Ficção Científica",
  "Drama",
  "Humor",
  "Ação",
  "Aventura"
]

client_list = [
   "Danilo",
   "Vitão",
   "Catherine",
   "Marcão",  
   "Jamilly",
   "Gabriel",
   "Cezario",
   "Rebeca",
   "Carla",
   "Carol",
   "Vinicius",
   "Iago",
   "Lucas",


]

author_list = [
     "Iury",
     "Eva",
     "Vini",
     "Laura",
     "Israel",
     "Wesley",
     "João Paulo",
     "Studart",
     "João Paulo",
     "Gusta",
     "Arnaldo",
     "Tomás",
     "Jô",
     "Andrea",
     "Tayllan",
     "Ítalo"

]

book_list = [
    "Orgulho e Preconceito",
    "1984",
    "Dom Quixote de la Mancha",
    "O Pequeno Príncipe",
    "Dom Casmurro",
    "O Bandolim do Capitão Corelli",
    "O Conde de Monte Cristo",
    "Um Estudo em Vermelho",
    "O Processo",
    "Cem Anos de Solidão",
    "O Coração das Trevas",
    "Eu, Robô",
    "O Senhor dos Anéis",
    "Guerra e Paz",
    "Grande Sertão: Veredas",
    "O Retrato de Dorian Gray",
    "A Redoma de Vidro",
    "A Hora da Estrela",
    "Ensaio Sobre a Cegueira",
    "Maurice",
    "Por quem os Sinos Dobram",
    "Hamlet",
    "Morte em Veneza",
    "Em Busca do Tempo Perdido ",
    "O Banquete",
    "Folhas de Relva",
    "On The Road: Pé na Estrada",
    "Uivo e outro poemas",
    "As Vinhas da Ira",
    "Pulp"
]


category_list.each do |name|
    Category.create( name: name)
  end


client_list.each do |name|
    Client.create( name: name)
  end


author_list.each do |name|
    Author.create( name: name)
  end

book_list.each do |title|
    Book.create( 
        title: title,
        author: Author.find(rand(1..15)),
        category: Category.find(rand(1..9)),
        stock: rand(1..50)

    )
  end



User.create(email:"admin@admin.com",password:"123456")